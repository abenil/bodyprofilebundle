<?php
/**
 * BodyProfileBundle
 *
 * This file is part of the BodyRecordBundle
 *
 * @author Nils Abegg <nils.abegg@gmail.com>
 */
namespace Abenil\BodyProfileBundle\Constants;

class BodyProfileConstants
{

    /**
     * @var string
     */
    const GENDER_MALE_ABBR = "m";

    /**
     * @var string
     */
    const GENDER_MALE = "male";

    /**
     * @var string
     */
    const GENDER_FEMALE_ABBR = "f";

    /**
     * @var string
     */
    const GENDER_FEMALE = "female";

    /**
     * @var string
     */
    const MEASUREMENT_UNIT_SYSTEM_DEFAULT = self::MEASUREMENT_UNIT_SYSTEM_METRIC;

    /**
     * @var string
     */
    const MEASUREMENT_UNIT_SYSTEM_METRIC = "metric";

    /**
     * @var string
     */
    const MEASUREMENT_UNIT_SYSTEM_IMPERIAL = "imperial";
}
