<?php
/**
 * BodyProfileBundle
 *
 * This file is part of the BodyRecordBundle
 *
 * @author Nils Abegg <nils.abegg@gmail.com>
 */
namespace Abenil\BodyRecordBundle\Controller;

use Abenil\BodyProfileBundle\Constants\BodyProfileConstants;
use Abenil\BodyRecordBundle\Constants\BodyRecordConstants;
use Abenil\BodyRecordBundle\Entity\BodyRecord;
use Abenil\BodyRecordBundle\Form\Type\BodyRecordType;
use Abenil\CommonBundle\Controller\RestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Form;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * BodyProfileRestController
 *
 * This class provides all the actions needed for the REST API of the BodyProfile.
 *
 * @package Abenil\BodyProfileBundle\Entity\Controller
 * @version 1.0.0
 * @RouteResource("BodyProfile")
 */
class BodyProfileRestController extends RestController
{

    /**
     * Cget action
     *
     * Returns the list of BodyProfiles.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Get("/body_profiles")
     */
    public function cgetAction()
    {
        return $this->returnErrorResponse('This function is not implemented.');
    }

    /**
     * Get action
     *
     * @param string $bodyProfileId path
     * @return View view instance
     * @Get("/body_profile/{bodyProfileId}")
     */
    public function getAction($bodyProfileId)
    {
        /**
         * @var BodyProfile
         */
        $bodyProfile = $this->getDoctrine()
            ->getRepository('AbenilBodyProfileBundle:BodyProfile')
            ->find($bodyProfileId);

        if ($bodyProfile == null) {
            return $this->returnErrorResponse('There is no profile for ID "' . $bodyProfileId . '"');
        }
        $ageCalculator = $this->get('abenil_body_profile.age_calculator');
        $bodyRecordArray = array (
            'id' => $bodyProfile->getId(),
            'age' => $ageCalculator->calculate($bodyProfile->getBirthday()),
            'height' => $bodyProfile->getWeight($this->measurementSystem),
            'gender' => $bodyProfile->getGender(),
        );

        return new Response(json_encode($bodyRecordArray));
    }

    /**
     * Post action
     *
     * Creates and persist a new BodyProfile.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Post("/body_profile/create")
     */
    public function postAction(Request $request)
    {

        $form = $this->getForm();
        $form->handleRequest($request);
        $bodyRecord = $form->getData();
        $debug = var_export($bodyRecord, true);
        return new Response($debug);
        if ($form->isValid()) {
            $bodyRecord = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($bodyRecord);
            $em->flush();

            return $this->returnSuccessResponse('The data was persisted.');
        }

        return $this->returnErrorResponse('The provided data is not valid.');
    }
}
