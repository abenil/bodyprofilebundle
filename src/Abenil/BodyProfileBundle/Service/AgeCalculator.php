<?php
/**
 * BodyProfileBundle
 *
 * This file is part of the BodyRecordBundle
 *
 * @author Nils Abegg <nils.abegg@gmail.com>
 */
namespace Abenil\BodyProfileBundle\Service;

/**
 * AgeCalculator
 *
 * This class calculates the age for a given dates.
 *
 * @package Abenil\BodyProfileBundle\Service
 * @version 1.0.0
 */
class AgeCalculator
{

    /**
     * Constructor
     *
     * Does nothing atm.
     */
    public function __construct()
    {
    }

    /**
     * Calculate
     *
     * Calculates the age of a user by the birthday.
     *
     * @param \DateTime $birthday
     *
     * @return integer
     */
    public function calculate(\DateTime $birthday)
    {

        $age = $birthday
            ->diff(new \DateTime('now', $birthday))
            ->y;

        return $age;
    }
}
