<?php
/**
 * BodyProfileBundle
 *
 * This file is part of the BodyRecordBundle
 *
 * @author Nils Abegg <nils.abegg@gmail.com>
 */
namespace Abenil\BodyProfileBundle\Entity;

use \Abenil\BodyRecordBundle\Entity\BodyRecordInterface;

/**
 * Interface BodyProfileInterface
 *
 * The interface for the BodyProfile.
 * For now it is only used for the entity,
 * but there might be coming some functionality
 * with a No-SQL DBMS.
 *
 * @package Abenil\BodyProfileBundle\Entity
 * @version 1.0.0
 */
interface BodyProfileInterface
{

    /**
     * Get ID
     *
     * Returns the ID.
     *
     * @return integer
     */
    public function getId();

    /**
     * Get height
     *
     * Returns the height.
     *
     * @return string
     */
    public function getHeight();

    /**
     * Get gender
     *
     * Returns the gender.
     *
     * @return string
     */
    public function getGender();

    /**
     * Get age
     *
     * Returns the age.
     *
     * @return integer
     */
    public function getAge();

    /**
     * Get birthday
     *
     * Returns the birthday.
     *
     * @return \DateTime
     */
    public function getBirthday();

    /**
     * Get measurement unit system
     *
     * Returns the measurement unit system - "metric" or "imperial"
     *
     * @return string
     */
    public function getMeasurementUnitSystem();

    /**
     * Get BodyRecords
     *
     * Returns the BodyRecords which are related to the BodyProfile.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBodyRecords();

    /**
     * Set height
     *
     * Sets the height.
     *
     * @param string $height
     * @return \Abenil\BodyProfileBundle\Entity\BodyProfile
     */
    public function setHeight($height);

    /**
     * Set gender
     *
     * Sets the gender abbreviation.
     *
     * @param string $gender
     * @return \Abenil\BodyProfileBundle\Entity\BodyProfile
     */
    public function setGender($gender);

    /**
     * Set birthday
     *
     * Sets the birthday.
     *
     * @param \DateTime $birthday
     * @return \Abenil\BodyProfileBundle\Entity\BodyProfile
     */
    public function setBirthday($birthday);

    /**
     * Set measurement unit system
     *
     * Sets the measurement unit system - "metric" or "imperial"
     *
     * @param string $measurementUnitSystem
     * @return \Abenil\BodyProfileBundle\Entity\BodyProfile
     */
    public function setMeasurementUnitSystem($measurementUnitSystem);

    /**
     * Add BodyRecord
     *
     * Adds a related BodyRecord.
     *
     * @param \Abenil\BodyRecordBundle\Entity\BodyRecordInterface $bodyRecord
     * @return BodyProfile
     */
    public function addBodyRecord(BodyRecordInterface $bodyRecord);

    /**
     * Remove BodyRecord
     *
     * Removes a related BodyRecord.
     *
     * @param \Abenil\BodyRecordBundle\Entity\BodyRecordInterface $bodyRecord
     */
    public function removeBodyRecord(BodyRecordInterface $bodyRecord);
}
