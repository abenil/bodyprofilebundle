<?php
/**
 * BodyProfileBundle
 *
 * This file is part of the BodyRecordBundle
 *
 * @author Nils Abegg <nils.abegg@gmail.com>
 */
namespace Abenil\BodyProfileBundle\Entity;

use Abenil\BodyProfileBundle\Constants\BodyProfileConstants;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * BodyProfile
 *
 * Holds the data for a BodyProfile.
 * A BodyProfile is the profile of a user with
 * all the data related to his body.
 *
 * @package Abenil\BodyProfileBundle\Entity\BodyProfile
 * @version 1.0.0
 * @ORM\Entity
 * @ORM\Table(name="body_profile")
 */
class BodyProfile implements BodyProfileInterface
{

    /**
     * ID
     *
     * The ID of the BodyProfile.
     *
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Birthday
     *
     * The birthday of the user.
     *
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $birthday;

    /**
     * Gender
     *
     * The gender abbreviation of the user.
     *
     * @var string
     * @ORM\Column(type="string")
     */
    protected $gender;

    /**
     * Height
     *
     * The height of the user.
     *
     * @var float
     * @ORM\Column(type="decimal", precision=3, scale=2, name="height")
     */
    protected $height;

    /**
     * Measurement unit system
     *
     * The measurement unit system - "metric" or "imperial".
     *
     * @var string
     * @ORM\Column(type="string")
     */
    protected $measurementUnitSystem = BodyProfileConstants::MEASUREMENT_UNIT_SYSTEM_DEFAULT;

    /**
     * BodyRecords
     *
     * The related BodyRecords.
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="\Abenil\BodyRecordBundle\Entity\BodyRecord", mappedBy="bodyProfile")
     **/
    protected $bodyRecords;

    /**
     * Constructor
     *
     * Sets the ArrayCollection.
     */
    public function __construct()
    {
        $this->bodyRecords = new ArrayCollection();
    }

    /**
     * Get ID
     *
     * Returns the ID.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set birthday
     *
     * Sets the birthday.
     *
     * @param \DateTime $birthday
     * @return \Abenil\BodyProfileBundle\Entity\BodyProfile
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * Returns the birthday.
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Get age
     *
     * Returns the age.
     *
     * @return integer
     */
    public function getAge()
    {
        $age = $this->birthday
            ->diff(new \DateTime('now', $this->birthday))
            ->y;

        return $age;
    }

    /**
     * Set gender
     *
     * Sets the gender abbreviation.
     *
     * @param string $gender
     * @return \Abenil\BodyProfileBundle\Entity\BodyProfile
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * Returns the gender.
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set measurement unit system
     *
     * Sets the measurement unit system - "metric" or "imperial"
     *
     * @param string $measurementUnitSystem
     * @return \Abenil\BodyProfileBundle\Entity\BodyProfile
     */
    public function setMeasurementUnitSystem($measurementUnitSystem)
    {
        $this->measurementUnitSystem = $measurementUnitSystem;

        return $this;
    }

    /**
     * Get measurement unit system
     *
     * Returns the measurement unit system - "metric" or "imperial"
     *
     * @return string
     */
    public function getMeasurementUnitSystem()
    {
        return $this->measurementUnitSystem;
    }

    /**
     * Add BodyRecord
     *
     * Adds a related BodyRecord.
     *
     * @param \Abenil\BodyRecordBundle\Entity\BodyRecordInterface $bodyRecord
     * @return BodyProfile
     */
    public function addBodyRecord(\Abenil\BodyRecordBundle\Entity\BodyRecordInterface $bodyRecord)
    {
        $this->bodyRecords[] = $bodyRecord;

        return $this;
    }

    /**
     * Remove BodyRecord
     *
     * Removes a related BodyRecord.
     *
     * @param \Abenil\BodyRecordBundle\Entity\BodyRecordInterface $bodyRecord
     */
    public function removeBodyRecord(\Abenil\BodyRecordBundle\Entity\BodyRecordInterface $bodyRecord)
    {
        $this->bodyRecords->removeElement($bodyRecord);
    }

    /**
     * Get BodyRecords
     *
     * Returns the BodyRecords which are related to the BodyProfile.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBodyRecords()
    {
        return $this->bodyRecords;
    }

    /**
     * Set height
     *
     * Sets the height.
     *
     * @param string $height
     * @return \Abenil\BodyProfileBundle\Entity\BodyProfile
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * Returns the height.
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }
}
