<?php
/**
 * BodyProfileBundle
 *
 * This file is part of the BodyRecordBundle
 *
 * @author Nils Abegg <nils.abegg@gmail.com>
 */
namespace Abenil\BodyProfileBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AbenilBodyProfileBundle
 *
 * The class for the AbenilBodyProfileBundle.
 *
 * @package Abenil\BodyProfileBundle
 * @version 1.0.0
 */
class AbenilBodyProfileBundle extends Bundle
{
}
